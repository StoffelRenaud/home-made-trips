import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class TripsService {
  public trips = this.setTrips();

  constructor() {
  }

  public setTrips() {
    return([
      {
        'owner': {
          'id':         1,
          'firstname':  'Renaud',
          'lastname':   'Stoffel'
        },
        'title': 'Superbe',
        'steps': [{
          'id':   1,
          'city': 'Paris',
        }, {
          'id':   2,
          'city': 'Nantes',
        }, {
          'id':   3,
          'city': 'Marseilles'
        }]
      }, {
        'owner': {
          'id':         2,
          'firstname':  'Thérèse',
          'lastname':   'Stoffel'
        },
        'title': 'Mouais',
        'steps': [{
          'id':   1,
          'city': 'Inde',
        }, {
          'id':   2,
          'city': 'Grèce',
        }, {
          'id':   3,
          'city': 'Albanie'
        }]
      }, {
        'owner': {
          'id':         3,
          'firstname':  'Vincent',
          'lastname':   'Raynaud'
        },
        'title': 'Fantastique',
        'steps': [{
          'id':   1,
          'city': 'USA',
        }, {
          'id':   2,
          'city': 'Canada',
        }, {
          'id':   3,
          'city': 'Cambodge'
        }]
      }, {
        'owner': {
          'id':         4,
          'firstname':  'Jean',
          'lastname':   'Bob'
        },
        'title': 'Ouf',
        'steps': [{
          'id':   1,
          'city': 'Israel',
        }, {
          'id':   2,
          'city': 'Inde',
        }, {
          'id':   3,
          'city': 'Japon'
        }]
      }, {
        'owner': {
          'id':         5,
          'firstname':  'Alexander',
          'lastname':   'Lundgreen'
        },
        'title': 'Coooooool',
        'steps': [{
          'id':   1,
          'city': 'Nantes',
        }, {
          'id':   2,
          'city': 'Bruxelles',
        }, {
          'id':   3,
          'city': 'Arta'
        }]
      }, {
        'owner': {
          'id':         6,
          'firstname':  'Jules',
          'lastname':   'Morin'
        },
        'title': 'Je conseille',
        'steps': [{
          'id':   1,
          'city': 'Bordeaux',
        }, {
          'id':   2,
          'city': 'Berlin',
        }, {
          'id':   3,
          'city': 'Viennes'
        }]
      }, {
        'owner': {
          'id':         7,
          'firstname':  'Nicolas',
          'lastname':   'Goyard'
        },
        'title': 'Youhou',
        'steps': [{
          'id':   1,
          'city': 'Lille',
        }, {
          'id':   2,
          'city': 'Budapest',
        }, {
          'id':   3,
          'city': 'Tunis'
        }]
      }
    ]);
  }

  public getUsers(): Array<Object> {
    const users: Array<Object> = [];
    for (let i = 0; i < this.trips.length; i++) {
      users.push(this.trips[i]['owner']);
    }
    return(users);
  }
}
