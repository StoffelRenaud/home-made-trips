import { Component, OnInit } from '@angular/core';
import { TripsService } from './../../services/trips/trips.service';

@Component({
  selector: 'app-trips',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.scss']
})
export class TripsComponent implements OnInit {

  constructor(private tripsService: TripsService) {
  }

  ngOnInit() {
  }

  public sortAsc(listArray: Array<Object>, data: string): Array<Object> {
    return listArray.sort(function(first: Object, sec: Object) {
      return first[data].localeCompare(sec[data]);
    });
  }

  public sortDesc(listArray: Array<Object>, data: string): Array<Object> {
    return listArray.sort(function(first: Object, sec: Object) {
      return sec[data].localeCompare(first[data]);
    });
  }
}
