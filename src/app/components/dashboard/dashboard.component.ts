import { Component, OnInit } from '@angular/core';
import { TripsService } from '../../services/trips/trips.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public users: Array<Object>;

  constructor(private tripsService: TripsService) {
  }

  ngOnInit() {
    this.users = this.tripsService.getUsers();
  }
}
